using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class ToDo : Note
    {
        public List<Note> errands = new List<Note>();

        public void addErrand(Note newErrand)
        {
            errands.Add(newErrand);
        }

        public void deleteErrand(int n)
        {
            errands.RemoveAt(n);
        }

        public Note getErrand(int n)
        {
            return errands[n];
        }

        public List<Note> getErrandList()
        {
            return errands;
        }

        public void DoTopPriority()
        {
            int min = errands[0].getPriority();

            for (int i = 0; i < errands.Count(); i++)
            {
                if (errands[i].getPriority() < min)
                {
                    min = errands[i].getPriority();
                }
            }

            for (int i = 0; i < errands.Count(); i++)
            {
                if (errands[i].getPriority() == min)
                {
                    deleteErrand(i);
                }
            }
        }

        public override string ToString()
        {
            string output = " ";
            for (int i = 0; i < errands.Count; i++)
            {
                output += errands[i].ToString() + "\n";
            }
            return output;
        }
    }
}
