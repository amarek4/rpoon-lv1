using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Note
    {
        private String text;
        private String author;
        private int priority;

        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getPriority() { return this.priority; }

        public void setText(string text) { this.text = text; }
        private void setAuthor(string author) { this.author = author; }
        public void setPriority(int priority) { this.priority = priority; }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }

        public Note(string text, string author, int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }

        public Note(string text, int priority)
        {
            this.text = text;
            this.author = "George R.R.Martin";
            this.priority = priority;
        }

        public Note()
        {
            this.text = "Winter is here";
            this.author = "George R.R.Martin";
            this.priority = 5;
        }

        public override string ToString()
        {
            return this.text + ", " + this.author + ", " + this.priority;
        }
    }
}
