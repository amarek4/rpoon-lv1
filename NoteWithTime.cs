using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class NoteWithTime : Note
    {
        private DateTime time;

        public NoteWithTime()
        {
            this.time = DateTime.Now;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public NoteWithTime(string text, string author, int priority, DateTime time)
        {
            this.time = time;
        }

        public override string ToString()
        {
            return base.ToString() + ", " + this.time;
        }
    }
}
