using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("Valar morghulis", "George R.R.Martin", 3);
            Console.WriteLine(note1.getAuthor());
            Console.WriteLine(note1.getText());

            Note note2 = new Note("Valar dohaeris", 2);
            Console.WriteLine(note2.getAuthor());
            Console.WriteLine(note2.getText());

            Note note3 = new Note();
            Console.WriteLine(note3.getAuthor());
            Console.WriteLine(note3.getText());

            NoteWithTime noteTime = new NoteWithTime();
            Console.WriteLine(noteTime.ToString());

            ToDo ToDoList = new ToDo();
            ToDoList.addErrand(note1);
            ToDoList.addErrand(note2);
            ToDoList.addErrand(note3);


            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Text:");
                string text = Console.ReadLine();

                Console.WriteLine("Author:");
                string author = Console.ReadLine();

                Console.WriteLine("Priority:");
                string priority = Console.ReadLine();

                Note newErrand = new Note(text, author, Int32.Parse(priority));

                ToDoList.addErrand(newErrand);
            }

            Console.WriteLine(ToDoList.ToString());
            ToDoList.DoTopPriority();
            Console.WriteLine(ToDoList.ToString());
        }
    }
}